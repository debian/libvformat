\input texinfo   @c -*-texinfo-*-
@c %**start of header
@setfilename libvformat.info
@settitle libvformat
@setchapternewpage odd
@c %**end of header

@ifinfo
This file documents libvformat

Copyright 2001 Nick Marley <Tilda@@indigo8.freeserve.co.uk>

licensed under the LGPL license.
@end ifinfo

@c  This title page illustrates only one of the
@c  two methods of forming a title page.

@dircategory Libraries
@direntry
* libvformat: (libvformat). Library to manipulate vcards
@end direntry

@titlepage
@title libvformat
@subtitle library to access versit cards
@author Nick <Tilda@@indigo8.freeserve.co.uk>
@end titlepage


@ifnottex
@node Top,  , (dir), (dir)
@top TITLE

This document describes libvformat

This document applies to version 1.13
of the program named libvformat
@end ifnottex

@menu
* Functions of libvformat::     description of interface.
* Public structures::           public structures of libvformat
* Function index::              index.
@end menu

@include libvformat-1.texi
@include libvformat-2.texi
@include libvformat-i.texi

@bye
