/******************************************************************************

    (C) Nick Marley, 2001 -

    This software is distributed under the GNU Lesser General Public Licence.
    Please read and understand the comments at the top of vf_iface.h before use!

FILE
    $Workfile$
    $Revision: 1.3 $
    $Author: tilda $
         
ORIGINAL AUTHOR
    Nick Marley

DESCRIPTION
    Platform independant test harness for the vformat library functions.  This
    code runs various tests on the library, results are written to stdout.

REFERENCES
    (none)    

MODIFICATION HISTORY
 *  $Log: split_phonebook.c,v $
 *  Revision 1.3  2001/10/24 18:37:38  tilda
 *  BASE64 bugfixes.  Split reader/writer code. Start create/modify work.
 *
 *  Revision 1.2  2001/10/24 05:29:24  tilda
 *  Tidy up.
 *
 *  Revision 1.1.1.1  2001/10/16 05:49:57  tilda
 *  Initial Import to CVS.
 *
 *******************************************************************************/

#ifndef NORCSID
static const char split_phonebook_c_vss_id[] = "$Header: /cvsroot/vformat/build/samples/split_phonebook.c,v 1.3 2001/10/24 18:37:38 tilda Exp $";
#endif

/*=============================================================================*
 ANSI C & System-wide Header Files
 *=============================================================================*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*============================================================================*
 Interface Header Files
 *============================================================================*/

#include "vformat/vf_iface.h"

/*============================================================================*
 Local Header File
 *============================================================================*/

#include "split_phonebook.h"

/*============================================================================*
 Private Defines
 *============================================================================*/
/* None */

/*============================================================================*
 Private Data Types
 *============================================================================*/
/* None */

/*============================================================================*
 Private Function Prototypes
 *============================================================================*/
/* None */

/*============================================================================*
 Private Data
 *============================================================================*/
/* None */

/*============================================================================*
 Public Function Implementations
 *============================================================================*/


/*----------------------------------------------------------------------------*
 * NAME
 *      split_phonebook()
 * 
 * DESCRIPTION
 *      Read in the indicated file which is assumed to be a list of VCARDs.
 *      The component cards are written out to individual files.
 *
 * RETURNS
 *      TRUE <=> read / written OK.
 *----------------------------------------------------------------------------*/

bool_t split_phonebook(
    const char *p_filename,
    const char *p_outdir
    )
{
    VF_OBJECT_T *p_object = NULL;
    bool_t ret = FALSE;

    if (vf_read_file(&p_object, p_filename))
    {
        VF_OBJECT_T *p_tmp = p_object;

        ret = TRUE;

        do
        {
            VF_PROP_T *p_prop;
            char name[256];

            name[0] = '\0';

            if (vf_get_property(&p_prop, p_object, FALSE, NULL, VFP_NAME, NULL))
            {
                const char *p_givenname = vf_get_prop_value_string(p_prop, VFP_NAME_GIVEN);
                const char *p_familyname = vf_get_prop_value_string(p_prop, VFP_NAME_FAMILY);

                if (p_familyname && p_givenname)
                {
                    sprintf(name, "%s %s", p_givenname, p_familyname);
                }
                else
                if (p_familyname)
                {
                    strcpy(name, p_familyname);
                }
                else
                if (p_givenname)
                {
                    strcpy(name, p_givenname);
                }
            }
            else
            if (vf_get_property(&p_prop, p_object, FALSE, NULL, VFP_FULLNAME, NULL))
            {
                const char *p_fullname = vf_get_prop_value_string(p_prop, VFP_NAME_GIVEN);

                if (p_fullname)
                {
                    strcpy(name, p_fullname);
                }
            }

            if (name[0])
            {
                char filename[_MAX_PATH];

                sprintf(filename, "%s\\%s.vcf", p_outdir, name);

                ret &= vf_write_file(filename, p_object, FALSE);
            }
        }
        while (ret && vf_get_next_object(&p_object))
            ;

        vf_delete_object(p_tmp, TRUE);
    }

    return ret;
}



/*============================================================================*
 Private Function Implementations
 *============================================================================*/
